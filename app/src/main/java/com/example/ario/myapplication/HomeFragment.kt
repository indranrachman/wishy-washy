package com.example.ario.myapplication

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.activity_home.*

class HomeFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) =
            inflater.inflate(R.layout.activity_home, container, false)!!

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        layoutShoes.setOnClickListener {
            val intent = Intent(activity, MerchantActivity::class.java)
            startActivity(intent)
        }

        layoutHelmet.setOnClickListener {
            val intent = Intent(activity, MerchantActivity::class.java)
            startActivity(intent)
        }

        layoutJacket.setOnClickListener {
            val intent = Intent(activity, MerchantActivity::class.java)
            startActivity(intent)
        }

        layoutPromo.setOnClickListener {
            val intent = Intent(activity, MerchantActivity::class.java)
            startActivity(intent)
        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        return if (id == R.id.action_settings) {
            true
        } else super.onOptionsItemSelected(item)

    }
}


