package com.example.ario.myapplication

import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentTransaction
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupBottomNav()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        return if (id == R.id.action_settings) {
            true
        } else super.onOptionsItemSelected(item)

    }

    //Fragment = menu atau navigasi yang didalam aplikasi, Activity =

    private fun setupBottomNav() {
        navigation.setOnNavigationItemSelectedListener { item ->

            var selected: Fragment? = null

            when (item.itemId) {
                R.id.action_item1 -> {
                    selected = HomeFragment()
                }
                R.id.action_item2 -> {
                    selected = HistoryFragment()
                }

                R.id.action_item3 -> {
                    selected = HelpMenuActivity()
                }

                R.id.action_item4 -> {
                    selected = MyAccountActivity()
                }
            }
            supportFragmentManager.beginTransaction()
                    .replace(R.id.container_home, selected)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE).commit()
            true
        }
    }
}
