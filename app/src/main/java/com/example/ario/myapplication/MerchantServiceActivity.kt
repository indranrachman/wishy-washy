package com.example.ario.myapplication

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import kotlinx.android.synthetic.main.activity_merchant_service.*
import kotlinx.android.synthetic.main.activity_select_merchant.*

class MerchantServiceActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_merchant_service)

        textSeeReviewLabel.setOnClickListener{
            //setContentView(R.layout.activity_customer_review)
            val intent = Intent (this@MerchantServiceActivity, CustomerReviewActivity::class.java)
            startActivity(intent)
        }

        btnContinueEst.setOnClickListener{
            //setContentView(R.layout.activity_merchant_service)
            val intent = Intent(this@MerchantServiceActivity, OrderReviewActivity::class.java)
            startActivity(intent)
        }

        toolbarMerchantService.setOnClickListener{
            //setContentView(R.layout.activity_select_merchant)
            onBackPressed()
        }

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        return if (id == R.id.action_settings) {
            true
        } else super.onOptionsItemSelected(item)

    }
}
